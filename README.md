
# ad-hoc vpn

Using `vagrant up` one can spin up a virtual machine on vagrant available 
providers and setup openvpn with a static key there. Useful for ephemeral vpn 
jump hosts. An environment variable with the provider token has to be set.


## network manager config

```conf
[connection]
id=ad-hoc
type=vpn
autoconnect=false

[vpn]
cipher=AES-256-CBC
comp-lzo=yes
connection-type=static-key
local-ip=10.9.8.2
remote-ip=10.9.8.1
static-key=/tmp/static.key
service-type=org.freedesktop.NetworkManager.openvpn

[vpn-secrets]
no-secret=true

[ipv4]
method=auto

```

## debian static key bug

    OpenSSL: error:24064064:random number generator:RAND_bytes:PRNG not seeded

https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=854914

either remove `/var/lib/openvpn` or create {u,}random device files in the 
chroot directory i.e.

```
mknod random c 1 8
mknod urandom c 1 9
```

